﻿using Integrity.Attributes;
using Integrity.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Integrity.Mapper
{
    public static class IntegrityMapperRepository
    {
        #region Public Methods
        public static bool Insert<T>(this IDbConnection connection, T entity)
        {
            try
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CreateInsertCommand(entity);
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool Delete<T>(this IDbConnection connection, T entity)
        {
            try
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CreateDeleteCommand(entity);
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                return true;
            }
            catch 
            {
                throw;
            }
        }

        public static bool Update<T>(this IDbConnection connection, T entity)
        {
            try
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CreateUpdateCommand(entity);
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                return true;
            }
            catch
            {
                throw;
            }
        }

        public static bool QueryFirstOrDefault<T>(this IDbConnection connection, object entity)
        {
            try
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CreateSelectCommand<T>();
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                return true;
            }
            catch 
            {
                throw;
            }
        }

        public static IEnumerable<T> GetAll<T>(this IDbConnection connection)
        {
            IList<T> list = new List<T>();
            Type t = typeof(T);

            try
            {
                string seed = IntegritySecurityQueryLanguage.GenerateSeedFromEntity<T>();

                var command = connection.CreateCommand();
                command.CreateSelectCommand<T>();
                connection.Open();

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    T obj = (T)Activator.CreateInstance(t);
                    t.GetProperties().ToList().ForEach(p =>
                    {
                        string entropiedSeed = IntegritySecurityQueryLanguage.IncreaseSeedEntropy(seed, p.Name);
                        p.SetValue(obj, IntegritySecurityQueryLanguage.UnobfuscateData(reader[p.Name], entropiedSeed));
                    });

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                throw;
            }
        }

        public static T GetByKey<T>(this IDbConnection connection, object key)
        {
            IList<T> list = new List<T>();
            Type t = typeof(T);

            try
            {
                string seed = IntegritySecurityQueryLanguage.GenerateSeedFromEntity<T>();

                var command = connection.CreateCommand();
                command.CreateSelectByKeyCommand<T>(key);
                connection.Open();

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    T obj = (T)Activator.CreateInstance(t);
                    t.GetProperties().ToList().ForEach(p =>
                    {
                        string entropiedSeed = IntegritySecurityQueryLanguage.IncreaseSeedEntropy(seed, p.Name);
                        p.SetValue(obj, IntegritySecurityQueryLanguage.UnobfuscateData(reader[p.Name], entropiedSeed));
                    });

                    list.Add(obj);
                }

                return list.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Private Methods
        private static void CreateInsertDatabaseParameters<T>(this IDbCommand command, T entity)
        {
            IDictionary<string, object> parameters = AttributesUtils.CreateParamValueDictionaryFromEntity(entity);
            string seed = IntegritySecurityQueryLanguage.GenerateSeedFromEntity<T>();

            foreach (var (key, value) in parameters)
            {
                var dbParameter = command.CreateParameter();

                dbParameter.ParameterName = key;

                string entropiedSeed = IntegritySecurityQueryLanguage.IncreaseSeedEntropy(seed, key);
                dbParameter.Value = IntegritySecurityQueryLanguage.ObfuscateData(value, entropiedSeed) ?? DBNull.Value;

                command.Parameters.Add(dbParameter);
            }
        }

        private static void CreateUpdateDatabaseParameters<T>(this IDbCommand command, T entity)
        {
            IDictionary<string, object> parameters = AttributesUtils.CreateParamValueDictionaryFromEntity(entity);
            string seed = IntegritySecurityQueryLanguage.GenerateSeedFromEntity<T>();

            foreach (var (key, value) in parameters)
            {
                var dbParameter = command.CreateParameter();

                dbParameter.ParameterName = key;

                string entropiedSeed = IntegritySecurityQueryLanguage.IncreaseSeedEntropy(seed, key);
                dbParameter.Value = IntegritySecurityQueryLanguage.ObfuscateData(value, entropiedSeed) ?? DBNull.Value;

                command.Parameters.Add(dbParameter);
            }
        }

        private static void CreateDeleteDatabaseParameters<T>(this IDbCommand command, T entity)
        {
            //In this case, i'll work with the data unobfuscated
            IDictionary<string, object> parameters = AttributesUtils.CreateKeyParamValueDictionaryFromEntity(entity);

            foreach (var (key, value) in parameters)
            {
                var dbParameter = command.CreateParameter();

                dbParameter.ParameterName = key;
                dbParameter.Value = value ?? DBNull.Value;

                command.Parameters.Add(dbParameter);
            }
        }

        private static void CreateInsertCommand<T>(this IDbCommand command, T entity)
        {

            IList<PropertyInfo> props = typeof(T).GetProperties().ToList();
            IList<Column> columnAttr = AttributesUtils.GetAttributeListFromProps<Column>(props).ToList();

            string insert = String.Join(",", columnAttr?.Select(x => x.ColumnName));
            string values = String.Join(",", props?.Select(x => $"@{ x.Name}"));

            command.CommandText = @$"insert into ##TableNameMap ({insert}) values ({values})";
            command.ReplaceTableNameMap<T>();

            command.CreateInsertDatabaseParameters(entity);
        }

        private static void CreateSelectCommand<T>(this IDbCommand command)
        {
            string select = GenerateSelectStringByType<T>();
            command.CommandText = @$"select {select} from ##TableNameMap";
            command.ReplaceTableNameMap<T>();
        }

        private static void CreateSelectByKeyCommand<T>(this IDbCommand command, object key)
        {
            string select = GenerateSelectStringByType<T>();
            string where = GenerateWhereStringByKey<T>(key);
            command.CommandText = @$"select {select} from ##TableNameMap where {where}";
            command.ReplaceTableNameMap<T>();
        }

        private static void CreateUpdateCommand<T>(this IDbCommand command, T entity)
        {
            
            IList<PropertyInfo> props = typeof(T).GetProperties().ToList();
            IList<Column> columnAttr = AttributesUtils.GetAttributeListFromProps<Column>(props).ToList();

            string set = String.Join(",", props.Select(x => $"{x.Name} = @{x.Name}"));
            string where = GenerateWhereStringByType<T>();

            command.CommandText = @$"update ##TableNameMap set {set} where ({where})";

            command.ReplaceTableNameMap<T>();

            command.CreateUpdateDatabaseParameters(entity);
        }

        private static void CreateDeleteCommand<T>(this IDbCommand command, T entity)
        {
            string where = GenerateWhereStringByType<T>();

            command.CommandText = @$"delete from ##TableNameMap where {where}";
            command.ReplaceTableNameMap<T>();

            command.CreateDeleteDatabaseParameters(entity);
        }

        private static void ReplaceTableNameMap<T>(this IDbCommand command)
        {
            // Retrieving tableAttribute information
            Table tableAttribute = (Table)Attribute.GetCustomAttribute(typeof(T), typeof(Table));
            if (tableAttribute != null)
                command.CommandText = command.CommandText.Replace("##TableNameMap", tableAttribute.TableName);
            else
                throw new Exception("No table provided");

        }

        private static string GenerateWhereStringByType<T>()
        {
            IList<PropertyInfo> props = typeof(T).GetProperties().ToList();
            IList<PropertyInfo> propsKey = props.Where(prop => Attribute.IsDefined(prop, typeof(Key))).ToList();
            IList<Column> columnAttr = AttributesUtils.GetAttributeListFromProps<Column>(propsKey).ToList();
            return String.Join(" and ", columnAttr?.Select(x => $"{x.ColumnName} = @{x.ColumnName}"));
        }

        private static string GenerateWhereStringByKey<T>(object key)
        {
            IList<PropertyInfo> propsKey = key.GetType().GetProperties().ToList();
            return String.Join(" and ", propsKey?.Select(x => $"{x.Name} = {x.GetValue(key)}"));
        }

        private static string GenerateSelectStringByType<T>()
        {
            IList<PropertyInfo> props = typeof(T).GetProperties().ToList();
            IList<Column> columnAttr = AttributesUtils.GetAttributeListFromProps<Column>(props).ToList();
            return String.Join(",", columnAttr?.Select(x => x.ColumnName));
        }
        #endregion


    }
}
