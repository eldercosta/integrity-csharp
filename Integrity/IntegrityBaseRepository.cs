﻿using Integrity.Mapper;
using System.Collections.Generic;
using System.Data;
using Integrity.Lib;
using System.Linq;

namespace Integrity.Repository
{
    public abstract class IntegrityBaseRepository<T> where T : class
    {
        protected string _command = string.Empty;
        internal string _connectionName;
        internal DatabaseType _dbType;

        /// <summary>
        /// A Base Repository for Integrity ORM and Encrypt 
        /// </summary>
        /// <param name="schemaName"></param>
        /// <param name="dbType"></param>
        protected IntegrityBaseRepository(string schemaName, DatabaseType dbType)
        {
            _connectionName = schemaName;
            _dbType = dbType;
        }

        #region Protected Methods
        protected bool Add(T entity)
        {
            using (IDbConnection conn = IntegrityDatabaseUtils.GetConnection(_connectionName, _dbType))
            {
                try
                {
                    return conn.Insert(entity);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                }
            }
        }

        protected T Get(object key)
        {
            using (IDbConnection conn = IntegrityDatabaseUtils.GetConnection(_connectionName, _dbType))
            {
                try
                {
                    return conn.GetByKey<T>(key);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                }
            }
        }

        protected IEnumerable<T> GetAll()
        {
            using (IDbConnection conn = IntegrityDatabaseUtils.GetConnection(_connectionName, _dbType))
            {
                try
                {
                    return conn.GetAll<T>();
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                }
            }
        }

        protected bool Delete(object key)
        {
            using (IDbConnection conn = IntegrityDatabaseUtils.GetConnection(_connectionName, _dbType))
            {
                try
                {
                    T entity = Get(key);
                    return conn.Delete(entity);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                }
            }
        }

        protected bool Update(T entity)
        {
            using (IDbConnection conn = IntegrityDatabaseUtils.GetConnection(_connectionName, _dbType))
            {
                try
                {
                    return conn.Update(entity);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                }
            }
        }

        protected object Query(string command, string parameters)
        {
            throw new System.NotImplementedException();
        }

        protected K QueryFirstOrDefault<K>(object parameter = null)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}
