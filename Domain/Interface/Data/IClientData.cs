﻿using Domain.Entities.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interface.Data
{
    public interface IClientData
    {
        IList<ClientEntity> GetAllClient();
        ClientEntity GetById(int clientId);
        bool Save(ClientEntity client);
        bool Edit(ClientEntity client);
        bool Delete(int clientId);
    }
}
