﻿using Integrity.Attributes;
using System;

namespace Domain.Entities.Entity
{
    [Table("exampleTable")]
    public class ClientEntity
    {
        [Column("client_name")]
        public string client_name { get; set; }

        [Column("clientSince_date")]
        public DateTime clientSince_date { get; set; }

        [Column("cardHolder_name")]
        public string cardHolder_name { get; set; }

        [Column("card_number")]
        public string card_number { get; set; }

        [Column("cardExpiration_date")]
        public DateTime cardExpiration_date { get; set; }

    }
}
