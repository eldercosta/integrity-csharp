﻿using Domain.Enum;
using Domain.Interface.Data;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Infrastructure.Data
{
    public class Context : IContext
    {
        private readonly ConcurrentDictionary<string, IDbConnection> _connections = new ConcurrentDictionary<string, IDbConnection>();
        public IDbConnection this[string key, DatabaseType dbType] { get { return GetConnection(key.ToUpper(), dbType); } }
        
        public Context()
        {
        }

        public void Dispose()
        {
            foreach(var conexao in _connections.Values)
            {
                if(conexao.State == ConnectionState.Open)
                {
                    conexao.Close();
                }

                conexao?.Dispose();
            }

            GC.SuppressFinalize(this);
        }

        private IDbConnection GetConnection(string connectionName, DatabaseType dbType)
        {
            if (!_connections.ContainsKey(connectionName))
            {
                IDbConnection conexao;
                switch(dbType)
                {
                    case DatabaseType.MySql:
                        conexao = ConnectionUtils.CreateMySqlDbConnection(connectionName);
                        break;
                    case DatabaseType.SqlServer:
                        conexao = ConnectionUtils.CreateSqlServerDbConnection(connectionName);
                        break;
                    default:
                        return null;
                }
                _connections.TryAdd(connectionName, conexao);
            }
            return _connections[connectionName];
        }
    }
}
