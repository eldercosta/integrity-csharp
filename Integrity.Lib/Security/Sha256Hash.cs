﻿using System.Security.Cryptography;
using System.Text;

namespace Integrity.Lib
{
    public static class HashLib
    {
        /// <summary>
        /// Generate a Seed Hash over a passed seed
        /// </summary>
        /// <param name="seed"></param>
        /// <returns>Seed Hash 256 bits</returns>
        public static byte[] Generate256SeedHash(string seed)
        {
            byte[] seedHash = ComputeSha256Hash(seed);
            return seedHash; //256 bits
        }

        private static byte[] ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));
                //return ConvertByteToString(bytes);

                return bytes;
            }
        }

        private static string ConvertByteToString(byte[] byteData)
        {
            // Convert byte array to a string   
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < byteData.Length; i++)
            {
                builder.Append(byteData[i].ToString("x2"));
            }
            return builder.ToString();
        }
    }
}
