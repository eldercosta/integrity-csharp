﻿using System;

namespace Integrity.Attributes
{
    /// <summary>
    /// Defines table name on database
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class Table : Attribute
    {
        public string TableName { get; set; }

        public Table(string text)
        {
            TableName = text;
        }
    }
}
