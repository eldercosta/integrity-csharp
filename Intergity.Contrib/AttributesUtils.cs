﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Integrity.Attributes
{
    public static class AttributesUtils
    {
        public static IEnumerable<TAttribute> GetAttributeListFromProps<TAttribute>(IList<PropertyInfo> props) where TAttribute : Attribute
        {
            return props.SelectMany(p => p.GetCustomAttributes()).OfType<TAttribute>();
        }

        public static IDictionary<string, object> CreateParamValueDictionaryFromEntity<T>(T entity)
        {
            // Retrieving properties from entity (ORM)
            var properties = typeof(T).GetProperties().ToList();

            return GenerateParamDictionary(properties, entity);
        }

        public static IDictionary<string, object> CreateKeyParamValueDictionaryFromEntity<T>(T entity)
        {
            IList<PropertyInfo> props = typeof(T).GetProperties().ToList();
            IList<PropertyInfo> propsKey = props.Where(prop => Attribute.IsDefined(prop, typeof(Key))).ToList();

            return GenerateParamDictionary(propsKey, entity);
        }

        private static IDictionary<string, object> GenerateParamDictionary<T>(IList<PropertyInfo> props, T entity)
        {
            IDictionary<string, object> parameters = new Dictionary<string, object>();

            foreach (var property in props)
                parameters.Add(property.Name, property.GetValue(entity));

            return parameters;
        }
    }
}
