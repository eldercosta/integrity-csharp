﻿using System;

namespace Integrity.Attributes
{
    /// <summary>
    /// Specify that property is a primary key in database
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class Key : Attribute
    {
        public Key() { }
    }
}
